import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

public class TableEntry {

	Sensor up;
	Sensor low;
	int val;
	boolean isUpperUse = false;
	boolean isLowerUse = false;
	// List<Sensor> list = new ArrayList<Sensor>();
	// Hashtable<String, Boolean> hash = new Hashtable<String, Boolean>();
	private String listSen;

	// public void addToSensorList(Sensor s) {
	// hash.put(s.name, true);
	// }
	//
	// public String getSensorList() {
	//
	// Iterator iterator = hash.keySet().iterator();
	// String st = "";
	// while (iterator.hasNext()) {
	// String key = iterator.next().toString();
	// st += key;
	// }
	// return st;
	// }

	public TableEntry() {

	}

	public void setList(String list) {
		listSen = list;
	}

	public String getList() {
		return listSen;
	}

	public TableEntry(Sensor up, Sensor low, int val) {
		this.low = low;
		this.up = up;
		this.val = val;

	}

	public Sensor getUp() {
		return up;
	}

	public Sensor getLow() {
		return low;

	}

	public int getValue() {
		return val;
	}

	public boolean equals(TableEntry t) {
		return t.up == up && t.low == low && t.val == val;
	}

}
