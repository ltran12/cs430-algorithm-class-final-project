import java.io.*;
import java.util.Collection;
import java.util.Collections;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;
import java.io.FileNotFoundException;
import java.lang.Math.*;

public class ReadInput {

	private List<Sensor> sensors = new ArrayList<Sensor>();
	private List<Target> targets = new ArrayList<Target>();
	BufferedWriter bw;
	private String delims;

	// constructor
	public ReadInput(BufferedWriter bw) throws IOException {
		try {
			delims = " ";
			readFile(bw);
		} catch (FileNotFoundException ex) {
			System.out.println("FileNotFoundException caught");
		}

	}

	// read the text file from the scanner and create sensors, targets object
	// out of the file.
	public void readFile(BufferedWriter bw) throws IOException {
		ArrayList<String> dataInput = new ArrayList<String>();
		Sensor sensor = null;
		this.bw = bw;
		File inputFile = new File("test4.txt");
		Scanner fileScanner = new Scanner(new FileReader(inputFile));
		try {
			while (fileScanner.hasNext()) { // scan through lines
				String data = fileScanner.nextLine();
				String type = data.substring(0, 1); // Sensor/Target/Coordinates
				if (type.equals("s")) {
					createSensor(data);
				} else if (type.equals("t")) {
					createTarget(data);
				} else {
					String y2 = fileScanner.next();
					setYCoordinates(data, y2 + " " + fileScanner.next()); // get
																			// the
																			// y1
																			// and
					// y2 coords
				}
			}
		} finally {
			fileScanner.close();
		}
		getTargetsCover();
		printData();
		sortTarget(targets);
	}

	// create target and sensor
	private void createSensor(String sensor) {
		// Scanner lineScanner = new Scanner(sensor);
		// lineScanner.useDelimiter(" ");
		String val[] = sensor.split(delims);
		String name = val[0]; // skip label

		double x = Double.parseDouble(val[1]);
		double y = Double.parseDouble(val[2]);
		int cost = Integer.parseInt(val[3]);

		sensors.add(new Sensor(name, x, y, cost));
	}

	private void createTarget(String target) {
		String val[] = target.split(delims);
		String name = val[0]; // skip label
		double x = Double.parseDouble(val[1]);
		double y = Double.parseDouble(val[2]);

		targets.add(new Target(name, x, y));
	}

	double yUp, yLow;

	// parse the y1 and y2 line
	private void setYCoordinates(String y1, String y2) {
		String val[] = y1.split(delims);
		yUp = Double.parseDouble(val[1]);

		val = y2.split(delims);
		yLow = Double.parseDouble(val[1]);
		// System.out.println("Y1: " + yUp + " Y2: " + yLow);

	}

	public List<Sensor> listOfSensors() {
		return sensors;
	}

	public List<Target> getTarget() {
		return targets;
	}

	public void getTargetsCover() throws IOException {// adds the sensors that
														// are cover the target
		for (Target target : targets) {
			addSenorsInrange(target);
		}
	}

	// if the target is in range of a sensor, add that sensor into the sensor
	// list of the target.
	public void addSenorsInrange(Target target) throws IOException {
		for (Sensor sensor : sensors) {
			if (distance(sensor, target) <= 1) {
				if (sensor.y <= yLow) {
					target.addLower(sensor);
//					System.out.println("added x: " + sensor.x + " y:"
//							+ sensor.y + " Lower");
//					bw.append("added x: " + sensor.x + " y:" + sensor.y
//							+ " Lower");
//					bw.append("\n");
				} else if (sensor.y >= yUp) {
					target.addUpper(sensor);
//					System.out.println("added x: " + sensor.x + " y:"
//							+ sensor.y + " upper");
//					bw.append("added x: " + sensor.x + " y:" + sensor.y
//							+ " upper");
//					bw.append("\n");
				} else
					System.err.println("Invalid sensor y location");
			}
		}
	}

	public double distance(Sensor s, Target t) throws IOException {
		double x = Math.pow((s.x - t.x), 2);
		double y = Math.pow((s.y - t.y), 2);
		double distance = Math.sqrt(x + y);
//		System.out.println("Distance: " + distance);
//		bw.append("Distance: " + distance);
//		bw.append("\n");
		return distance;
	}

	public void printData() throws IOException {
		for (Sensor s : sensors) {
			System.out.println(s.toString());
//			bw.append(s.toString());
//			bw.append("\n");
		}
		for (Target t : targets) {
			System.out.println(t.toString());
//			bw.append(t.toString());
//			bw.append("\n");
		}
	}

	public void sortTarget(List<Target> tar) {
		Collections.sort(tar);
	}
}