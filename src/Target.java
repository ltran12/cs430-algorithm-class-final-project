import java.util.ArrayList;
import java.util.List;

// create target object
public class Target implements Comparable<Target> {
	double x;
	double y;
	String name;

	List<Sensor> upperSensors = new ArrayList<Sensor>();
	List<Sensor> lowerSensors = new ArrayList<Sensor>();

	public Target(String name, double x, double y) {
		this.x = x;
		this.y = y;
		this.name = name;
	}

	public double getXPosition() {
		return x;
	}

	public double getYPosition() {
		return y;
	}

	
	public List<Sensor> getUp() {
		// return the list of upper sensor that cover the target
		return upperSensors;
	}

	public List<Sensor> getLow() {
		// return the list of lower sensor cover the target
		return lowerSensors;
	}

	public void addUpper(Sensor s) {
		upperSensors.add(s);
	}

	public void addLower(Sensor s) {
		lowerSensors.add(s);
	}

	public String toString() {
		return "Target: " + name + " " + getXPosition() + "," + getYPosition();
	}

	public boolean equals(Target t) {
		return t.x == x || t.y == y;
	}

	@Override
	public int compareTo(Target o) {
		if (this.x < o.x)
			return -1;
		else if (x > o.x)
			return 1;
		else
			return 0;
	}
}