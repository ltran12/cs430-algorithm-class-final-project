import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Algorithm {
	private static List<TableEntry> OPT = new ArrayList<TableEntry>();
	private List<Target> tar;
	FileWriter fw;
	BufferedWriter bw;

	public Algorithm(List<Target> target) {
		this.tar = target;

	}

	public TableEntry runAlgo(BufferedWriter bw)
			throws TargetNotCoverException, IOException {
		List<List<TableEntry>> tables = new ArrayList<List<TableEntry>>();
		// List<TableEntry> firstable = new ArrayList<TableEntry>();
		Sensor nullSen = new Sensor("null", -100, -100, 0);

		// going through the list of target
		int counter = 0;
		boolean first = true;
		for (Target t : tar) {
			counter++;

			List<TableEntry> table = new ArrayList<TableEntry>();
			List<Sensor> upper = t.getUp();
			List<Sensor> lower = t.getLow();
			if (upper.size() == 0 && lower.size() == 0) {
				throw new TargetNotCoverException();
			}

			System.out.println("Target name: " + counter + " " + t.name + " "
					+ t);
			bw.append("Target name: " + counter + " " + t.name + " " + t);
			/*------------------------------------------------------------------------*/
			System.out.println("size of upper sensor list ------------- "
					+ upper.size() + " " + upper.toString());
			System.out.println("size of lower sensor list------------- "
					+ lower.size() + " " + lower.toString());
			// ----------------------------------------------------------------------------------------------------------------
			t.addLower(nullSen);
			t.addUpper(nullSen);
			//
			if (first) {
				// t.addLower(nullSen);
				// t.addUpper(nullSen);

				upper = t.getUp();
				lower = t.getLow();
				first = false;
				for (Sensor u : upper) {
					for (Sensor l : lower) {

						int newValue = u.cost + l.cost;

						// get the last entry in OPT
						TableEntry temp = new TableEntry(u, l, newValue);
						if (u.name.equals("null") && l.name.equals("null")) {
							continue;
						} else if (!u.name.equals("null"))
							temp.setList(" " + u.name + " ");
						else if (!l.name.equals("null"))
							temp.setList(" " + l.name + " ");

						table.add(temp);

						System.out.println("first newvalue = " + newValue
								+ "  " + u.name + l.name);
					}
				}

				tables.add(table);

				// ----------------------------------------------------------------------------------------------------------------
			} else {
				if (upper.size() == 0)
					t.addUpper(nullSen);
				if (lower.size() == 0)
					t.addLower(nullSen);

				for (Sensor u : t.getUp()) {
					for (Sensor l : t.getLow()) {

						List<TableEntry> tab = new ArrayList<TableEntry>();

						if (u.name.equals("null") && l.name.equals("null")) {
							continue;
						}
						bw.append("===================================\n");

						for (TableEntry entry : tables.get(tables.size() - 1)) {

							String senList = entry.getList();
							// System.out.println("list in tar " + t.name + " "
							// + senList);
							boolean fi = false;
							boolean se = false;
							TableEntry temp = new TableEntry();
							temp.up = u;
							temp.low = l;
							String list = senList;
							// temp.setList(senList);

							Sensor preUp = entry.getUp();
							Sensor preLow = entry.getLow();
							int newValue = entry.getValue();

							if (u.isDominanting(preUp, t.x)
									&& !senList.contains(" " + u.name + " ")) {
								fi = true;
								// if u is a new dominant -> add u
								newValue += u.getCost();
								// temp.setList(senList + u.name + " ");
								list += u.name + " ";
								// System.out.println("UPPER + " + u.name
								// + newValue);
								// bw.append("UPPER ADDED " + u.name + " "
								// + preUp.name + " " + list + newValue
								// + "\n");

							}

							if (l.isDominanting(preLow, t.x)
									&& !senList.contains(" " + l.name + " ")) {
								se = true;
								newValue += l.getCost();
								// temp.setList(senList + l.name + " ");
								list += l.name + " ";

								// System.out.println("LOWER + " + l.getCost());
								// bw.append("LOWER ADDED " + l.name + " "
								// + preLow + " " + list + " " + newValue
								// + "\n");

							}

							if (!fi) {
								temp.up = preUp;
							}
							if (!se) {
								temp.low = preLow;

							}

							temp.setList(list);
							temp.val = newValue;
							tab.add(temp);

							// System.out.println(" new value = " +
							// newValue
							// + " " + u.name + " " + l.name);

						}
						TableEntry min = findMin(tab);
						System.out.println(min.getUp().name);
						System.out.println(min.getLow().name);
						System.out.println(min.getList());
						System.out.println(min.val);

						table.add(min);
					}

				}

				// search for the min value in the table then add it to OPT list

				tables.add(table);
				System.out
						.println("------------------------------------------------------------------------------------------------");
			}

		}
		System.out.println("DONE \n\n\n");
		TableEntry t = findMin(tables.get(tables.size() - 1));
		System.out.println(t.getList());
		bw.append(t.getList());
		if (checkCoverage(tar, t.getList())) {
			System.out.println("correct !!!!!!!!!");
		}
		return t;

	}

	// find and return the minimum value inside of the tables
	public TableEntry findMin(List<TableEntry> table) {

		int x = Integer.MAX_VALUE;
		int k = 0;

		for (int i = 0; i < table.size(); i++) {
			TableEntry t = table.get(i);
			if (t != null && x > t.getValue()) {
				x = t.getValue();
				k = i;
			}
		}

		return table.get(k);
	}

	// check if every single targets are cover by the list of sensor
	public boolean checkCoverage(List<Target> tar, String listOfSensor) {
		ArrayList<Boolean> boo = new ArrayList<Boolean>();
		for (Target t : tar) {
			boolean a = false;
			List<Sensor> up = t.getUp();
			List<Sensor> low = t.getLow();
			for (Sensor u : up) {
				if (listOfSensor.contains(" " + u.name + " "))
					a = true;
			}
			for (Sensor l : low) {
				if (listOfSensor.contains(" " + l.name + " "))
					a = true;
			}
			boo.add(a);
		}
		for (Boolean b : boo) {
			if (!b)
				return false;
		}
		return true;
	}
}
