// sensor object
public class Sensor {
	int cost;
	double x;
	double y;
	String name;
	boolean inSPrime;

	public Sensor(String name, double x, double y, int cost) {
		this.cost = cost;
		this.x = x;
		this.y = y;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public int getCost() {
		return cost;
	}

	public double getXPosition() {
		return x;
	}

	public double getYPosition() {
		return y;
	}

	// compare and return if the current sensor is dominating the sensor in the
	public boolean isDominanting(Sensor u, double lineX) {
		// boolean dominating = true;
		
		if (this.name.equals("null"))
			return false;
		if (u.name.equals("null"))
			return true;
		if(name.equals(u.name)) return false;
		if (Math.abs(lineX - u.x) <= 1) {// if it intersects
			double[] myIntersectionPoints = pointsOfIntersection(this, lineX);
			double[] yourIntersectionPoints = pointsOfIntersection(u, lineX);
			if (this.y > 0) { // an upper disk ?
				if (myIntersectionPoints[0] > yourIntersectionPoints[0]) {
					return false; // i am dominating you
				} else if (myIntersectionPoints[0] == yourIntersectionPoints[0]) {
					if (this.x >= u.x) {
						return false;
					}
				}
			} else {// lower disk?
				if (myIntersectionPoints[0] < yourIntersectionPoints[0]) {
					return false; // i am not dominating you
				} else if (myIntersectionPoints[0] == yourIntersectionPoints[0]) {
					if (this.x >= u.x) {
						return false;
					}

				}
			}
		}
		return true;
	}

	// look for a point of intersect of the cover region
	public double[] pointsOfIntersection(Sensor s, double lineX) {
		double[] points = new double[2];

		points[0] = s.y + Math.sqrt(1 - Math.pow((lineX - s.x), 2));
		points[1] = s.y - Math.sqrt(1 - Math.pow((lineX - s.x), 2));
		return points;
	}

	public String toString() {
		return "  " + name  ;
	}

	public boolean equals(Sensor s) {
		return name.equals(s.name);
	}
}