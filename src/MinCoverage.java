import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MinCoverage {
	static List<Sensor> S;
	static double totalCost;

	public static void main(String[] args) throws TargetNotCoverException, IOException {
		// read the text file and create object
        File file = new File("output.txt");
        FileWriter fw;
        BufferedWriter bw;
        if (!file.exists()) {
            file.createNewFile();
        }

        fw = new FileWriter(file);
        bw = new BufferedWriter(fw);
		ReadInput read = new ReadInput(bw);

		S = read.listOfSensors();

		// run the actual algorithm
		Algorithm a = new Algorithm(read.getTarget());
		TableEntry ta = a.runAlgo(bw);
		System.out
				.print("The minimum cost to cover all of the targets in the list is: "
						+ ta.val);
        bw.append("\n");
        bw.append("The minimum cost to cover all of the targets in the list is: "
                + ta.val);
        bw.close();

	}

}
